#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn import tree

#Allow User to Insert their String Text
def detect(paragraph):
    date = []
    subject = []
    text = []
    title = []
    Cleaned = []
    Cleaned2 = []
    Score = 0
    
    label=[]

    fake =  open('Clean_Fake_Data.py','r') 
    true =  open('Clean_True_Data.py','r') 
    #Cleaning the data implemented for fake and true datasets
    lines = [[x.rstrip('\n')] for x in fake]
    lines.remove(lines[0])

    lines2 = [[x.rstrip('\n')] for x in true]
    lines2.remove(lines2[0])

    
    #removing lines that are not of length 3, which is title, text and subject, and date
    for i in range (len(lines)):
        rows = (((lines[i][0].strip('').split(',"'))))

        if len(rows)==3 and len(rows[1].split('",')[1]) < 20 :
            title.append(rows[0])
            #splitting text and subject
            text.append(rows[1].split('",')[0])
            subject.append(rows[1].split('",')[1])
            date.append(rows[-1][0:-1])
            label.append("Fake")
    #repeating for the true data
    for i in range (len(lines2)):
        rows = (((lines2[i][0].strip('').split(',"'))))

        if len(rows)==3 and len(rows[1].split('",')[1]) < 20 :
            title.append(rows[0])
            text.append(rows[1].split('",')[0])
            subject.append(rows[1].split('",')[1])
            date.append(rows[-1][0:-1])
            label.append("True")
            
    #splitting the data into training and testing stes
    #data used is text and label(which is if the text is fake or not)
    x_train,x_test,y_train,y_test=train_test_split(text, label, test_size=0.2, random_state=7)

        
    #finding what the most relevant repeated word is     
    tfidf_vectorizer=TfidfVectorizer(stop_words='english', max_df=0.7)
    #DataFlair - Fit and transform train set, transform test set
    #transforming the data 
    tfidf_train=tfidf_vectorizer.fit_transform(x_train) 
    tfidf_test=tfidf_vectorizer.transform(x_test)
    feature_names = np.array(tfidf_vectorizer.get_feature_names())
    
    #passive aggressive classifier for detecting 
    pac=PassiveAggressiveClassifier(max_iter=50)
    pac.fit(tfidf_train,y_train)
    
    #Decision Tree classifier for detecting 
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(tfidf_train,y_train)
    #K_Neighbour classifier for detecting 
    neigh = KNeighborsClassifier()
    neigh = neigh.fit(tfidf_train,y_train)
    #DataFlair - Predict on the test set and calculate accuracy
    
    #Predicting the text from the user for the passive aggressive classifier
    y_pred=pac.predict(tfidf_test)
    user = pac.predict(tfidf_vectorizer.transform([paragraph]))
    
    #If score is 50% or more then it is fake
    if 'Fake' in user:
        Score+=1
        
    print( user)
    score=accuracy_score(y_test,y_pred)
    print('Passive Aggressive Classifier Accuracy:', round(score*100,2),'%')
        
    model = confusion_matrix(y_test,y_pred, labels=['Fake','True'])
    print(model)
    #Predicting the text from the user for the decision tree classifier
    y_pred_tree=clf.predict(tfidf_test)
    score_tree=accuracy_score(y_test,y_pred_tree)
    tree_user = clf.predict(tfidf_vectorizer.transform([paragraph]))
    if 'Fake' in tree_user:
        Score+=1
        
    print( tree_user)
    print('Decision Tree Accuracy:', round(score_tree*100,2),'%')
        
    model = confusion_matrix(y_test,y_pred_tree, labels=['Fake','True'])
    print(model)
    #Predicting the text from the user for the K-Neigbour
    y_pred_K=neigh.predict(tfidf_test)
    score_K=accuracy_score(y_test,y_pred_K)
    K_user = neigh.predict(tfidf_vectorizer.transform([paragraph]))
    if 'Fake' in K_user:
        Score+=1
    print(K_user )
    print('K_Neighbour Classifier Accuracy:', round(score_K*100,2),'%')
        
    model = confusion_matrix(y_test,y_pred_K, labels=['Fake','True'])
    print(model)
    
    #Predicting the text from the user for the Logistic Regression
    logreg = LogisticRegression()
    logreg = logreg.fit(tfidf_train,y_train)
    y_pred_logreg=logreg.predict(tfidf_test)
    score_logreg=accuracy_score(y_test,y_pred_logreg)
    log_user = logreg.predict(tfidf_vectorizer.transform([paragraph]))
    if 'Fake' in log_user:
        Score+=1
    print(log_user )
    print('Logistic Regression Accuracy:', round(score_logreg*100,2),'%')
    
       
    model = confusion_matrix(y_test,y_pred_logreg, labels=['Fake','True'])
    print(model)
    
    if Score/4 >= 0.5:
        print("The Text Entered is Fake",Score)
    else:
        print("The Text Entered is True",Score)
        
    #This code was used for getting visualisation data for most frequent words
#     def get_top_tf_idf_words(response, top_n=2):
#         sorted_nzs = np.argsort(response.data)[:-(top_n+1):-1]
#         return feature_names[response.indices[sorted_nzs]][0]
#         #return feature_names[response.indices[sorted_nzs]][0],response.data[:-(top_n+1):-1]

#     freq_words = [get_top_tf_idf_words(response,1) for response in tfidf_test]

#     max_freq = []
#     # take second element for sort
#     def takeSecond(elem):
#         return elem[1]

#     for i in freq_words:
#         max_freq.append([i,freq_words.count(i)])
#     unique =[]    
#     for i in max_freq:
#         if i not in unique:
#             unique.append(i)

#     unique.sort(key = takeSecond)
#     repeated =[]
#     for i in range(11):
#         if i !=0:
#             repeated.append(unique[-i])

#     x_value = []
#     y_value =[]
#     for i in repeated:
#         x_value.append(i[0])
#         y_value.append(i[1])
#     plt.bar(x_value, y_value, color ='r', width = 0.2)
#     plt.xlabel("Number of Frequency")
#     plt.ylabel("Word")
#     plt.title("Most Repeated Words in Articles")

#     plt.savefig("Sample_Graph1.png")
detect("tax")




# In[ ]:




